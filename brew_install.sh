#!/usr/bin/env bash

path_to_brew=$(which brew)
if [[ ! -x "$path_to_brew" ]] ; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
fi

brew install pwgen
brew install htop
brew install wget
brew install python3
brew install tree
brew install unrar
brew install unzip
brew install calc
brew install ccrypt
brew install zip
brew install cask
brew install macvim
brew install mono
brew install node
brew install rust
brew install cmake
brew install R
brew install libconfig 
brew install readline 
brew install lua 
brew install libevent 
brew install jansson
brew install java
brew install mas
brew install libomp
brew install pandoc
brew install swig
brew install golang
brew install openmpi
brew install coreutils
brew install awscli
brew install yarn
brew install ipopt
brew install metis
brew install ufraw

brew tap oncletom/cpdf
brew install cpdf

brew tap mongodb/brew
brew install mongodb-community@4.2

brew update
brew upgrade

