#!/usr/bin/env bash

set -uexo pipefail
sudo launchctl unload -w /Library/LaunchDaemons/com.nekrald.clean.sys.apps.plist
sudo rm -rf /usr/local/bin/clean_sys_apps.sh
sudo rm -rf /Library/LaunchDaemons/com.nekrald.clean.sys.apps.plist 

