#!/usr/bin/env bash

if [ -d /Applications/Safari.app ]; then
    rm -rf /Applications/Safari.app
fi

if [ -d /System/Applications/Notes.app ]; then
    mount -uw /
    rm -rf /System/Applications/Notes.app
fi

if [ -d /System/Applications/News.app ]; then
    mount -uw /
    rm -rf /System/Applications/News.app
fi

if [ -d /System/Applications/TV.app ]; then
    mount -uw /
    rm -rf /System/Applications/TV.app
fi

if [ -d /System/Applications/Podcasts.app ]; then
    mount -uw /
    rm -rf /System/Applications/Podcasts.app
fi

if [ -d /System/Applications/Chess.app ]; then
    mount -uw /
    rm -rf /System/Applications/Chess.app
fi

if [ -d /System/Applications/Stocks.app ]; then
    mount -uw /
    rm -rf /System/Applications/Stocks.app
fi

if [ -d /System/Applications/Stickies.app ]; then
    mount -uw /
    rm -rf /System/Applications/Stickies.app
fi

if [ -d /System/Applications/Reminders.app ]; then
    mount -uw /
    rm -rf /System/Applications/Reminders.app
fi

if [ -d /System/Applications/Calendar.app ]; then
    mount -uw /
    rm -rf /System/Applications/Calendar.app
fi

if [ -d /System/Applications/Contacts.app ]; then
    mount -uw /
    rm -rf /System/Applications/Contacts.app
fi

if [ -d /System/Applications/Mail.app ]; then
    mount -uw /
    rm -rf /System/Applications/Mail.app
fi

if [ -d /System/Applications/Home.app ]; then
    mount -uw /
    rm -rf /System/Applications/Home.app
fi

if [ -d /System/Applications/Messages.app ]; then
    mount -uw /
    rm -rf /System/Applications/Messages.app
fi

if [ -d /System/Applications/FaceTime.app ]; then
    mount -uw /
    rm -rf /System/Applications/FaceTime.app
fi

if [ -d "/System/Applications/Voice Memos.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Voice Memos.app"
fi

if [ -d "/System/Applications/Photo Booth.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Photo Booth.app"
fi

if [ -d "/System/Applications/Books.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Books.app"
fi

if [ -d "/System/Applications/Music.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Music.app"
fi

if [ -d "/System/Applications/QuickTime Player.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/QuickTime Player.app"
fi

if [ -d "/System/Applications/VoiceMemos.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/VoiceMemos.app"
fi

if [ -d "/System/Applications/FindMy.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/FindMy.app"
fi

if [ -d "/System/Applications/Siri.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Siri.app"
fi

if [ -d "/System/Applications/Maps.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Maps.app"
fi

if [ -d "/System/Applications/Launchpad.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Launchpad.app"
fi

if [ -d "/System/Applications/App Store.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/App Store.app"
fi

if [ -d "/System/Applications/Image Capture.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Image Capture.app"
fi

if [ -d "/System/Applications/Preview.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Preview.app"
fi

if [ -d "/System/Applications/Photos.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Photos.app"
fi

if [ -d "/System/Applications/Dictionary.app" ]; then
    mount -uw /
    rm -rf "/System/Applications/Dictionary.app"
fi
