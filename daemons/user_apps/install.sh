#!/usr/bin/env bash

directory=`pwd`
echo ${director}
set -uexo pipefail
sudo cp "${directory}/clean_user_apps.sh"  /usr/local/bin/clean_user_apps.sh
sudo cp com.nekrald.clean.user.apps.plist /Library/LaunchDaemons/
sudo launchctl load -w /Library/LaunchDaemons/com.nekrald.clean.user.apps.plist

