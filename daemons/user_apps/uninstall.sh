#!/usr/bin/env bash

set -uexo pipefail
sudo launchctl unload -w /Library/LaunchDaemons/com.nekrald.clean.user.apps.plist
sudo rm -rf /usr/local/bin/clean_user_apps.sh
sudo rm -rf /Library/LaunchDaemons/com.nekrald.clean.user.apps.plist 

