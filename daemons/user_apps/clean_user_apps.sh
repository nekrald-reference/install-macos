#!/usr/bin/env bash

rm -rf /Applications/Telegram*.app
rm -rf /Applications/Google*Chrome*.app
rm -rf /Applications/Chromium*.app
rm -rf /Applications/Brave*.app
rm -rf /Applications/Vivaldi*.app
rm -rf /Applications/Opera*.app
rm -rf /Applications/Discord*.app
rm -rf /Applications/Sea*Monkey*.app
rm -rf /Applications/Omni*Web**.app
rm -rf /Applications/Edge*.app
rm -rf /Applications/Roccat*.app
rm -rf /Applications/Maxthon*.app
rm -rf /Applications/Dash*.app
rm -rf /Applications/Internet*Explorer*.app
rm -rf /usr/local/bin/lynx
rm -rf /usr/local/bin/links
rm -rf /usr/local/bin/elinks
rm -rf /usr/local/bin/links2
rm -rf /usr/local/bin/w3m

