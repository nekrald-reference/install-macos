#!/usr/bin/env bash

directory=`pwd`
echo ${director}
set -uexo pipefail
sudo cp "${directory}/clean_sys_utilities.sh"  /usr/local/bin/clean_sys_utilities.sh
sudo cp com.nekrald.clean.sys.utilities.plist /Library/LaunchDaemons/
sudo launchctl load -w /Library/LaunchDaemons/com.nekrald.clean.sys.utilities.plist

