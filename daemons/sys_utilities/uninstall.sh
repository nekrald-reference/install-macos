#!/usr/bin/env bash

set -uexo pipefail
sudo launchctl unload -w /Library/LaunchDaemons/com.nekrald.clean.sys.utilities.plist
sudo rm -rf /usr/local/bin/clean_sys_utilities.sh
sudo rm -rf /Library/LaunchDaemons/com.nekrald.clean.sys.utilities.plist 

