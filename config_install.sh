#!/usr/bin/env bash
# Processing .rc files.

set -uexo pipefail
execution_place=$(pwd)
cd $HOME
ln -sf "$execution_place/.rc" "$HOME/.rc"
cd "$HOME/.rc"
. link.sh
cd "$execution_place"

