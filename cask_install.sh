#!/usr/bin/env bash

set -uexo pipefail

brew cask install rstudio
brew cask install transmission
brew cask install vlc
brew cask install unshaky
brew cask install calibre
brew cask install udeler
brew cask install gimp
brew cask install mactex
brew cask install pycharm-ce
brew cask install skype
brew cask install skype-for-business
brew cask install microsoft-teams
brew cask install djview
brew cask install zoomus
brew cask install grammarly
brew cask install thunderbird
brew cask install vagrant
brew cask install firefox
brew cask install jupyter-notebook-viewer
brew cask install java
brew cask install microsoft-word
brew cask install microsoft-excel
brew cask install microsoft-powerpoint
brew cask install time-out
brew cask install all-in-one-messenger
brew cask install adobe-acrobat-reader
brew cask install slack
brew cask install caffeine
brew cask install google-earth-pro
brew cask install android-platform-tools
brew cask install evernote
brew cask install gfortran
brew cask install geogebra
brew cask install rawtherapee
brew cask install balenaetcher

brew cask upgrade

sudo spctl --master-disable
brew cask install virtualbox
sudo spctl --master-enable

set +uexo pipefail
