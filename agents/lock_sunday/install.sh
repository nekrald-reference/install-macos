#!/usr/bin/env bash
directory=`pwd`
echo ${director}
set -uexo pipefail
mkdir -p $HOME/Library/LaunchAgents
sudo ln -sf "${directory}/lock_sunday.py"  /usr/local/bin/lock_sunday.py
cp com.nekrald.lock.sunday.plist $HOME/Library/LaunchAgents/
launchctl load -w $HOME/Library/LaunchAgents/com.nekrald.lock.sunday.plist

