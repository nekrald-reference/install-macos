#!/usr/bin/env python3
import argparse
import logging
import subprocess as sp
from datetime import datetime
from datetime import timedelta


def main():
    """ Main entry point. """
    if _is_sunday():
        _minimize_finder()
        _lock_screen()
        _kill_terminal()


def _is_sunday() -> bool:
    """ Checks if it is Sunday today. """
    time_now = datetime.now()
    if time_now.weekday() == 6:
        return True
    return False


def _minimize_finder() -> None:
    """ Hides finder. """
    process = sp.Popen(['osascript', '-e',
        'tell application "Finder" to set collapsed of windows to true'],
        stderr=sp.PIPE)
    process.wait()


def _lock_screen() -> None:
    """ Locks screen. """
    process = sp.Popen([
        'osascript', '-e',
        'tell application "System Events"  to keystroke "q" using {command down,control down}'
    ], stderr=sp.PIPE)
    process.wait()


def _kill_terminal() -> None:
    """ Kills all terminal windows. """
    process = sp.Popen([
        'killall', 'Terminal'], stderr=sp.PIPE)
    process.wait()


if __name__ == '__main__':
    main()
