#!/usr/bin/env bash
set -uexo pipefail
launchctl unload -w $HOME/Library/LaunchAgents/com.nekrald.lock.sunday.plist
sudo unlink /usr/local/bin/lock_sunday.py
rm -rf $HOME/Library/LaunchAgents/com.nekrald.lock.sunday.plist 

