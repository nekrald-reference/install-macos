#!/usr/bin/env bash
directory=`pwd`
echo ${director}
set -uexo pipefail
mkdir -p $HOME/Library/LaunchAgents
sudo ln -sf "${directory}/lock_daily.py"  /usr/local/bin/lock_daily.py
cp com.nekrald.lock.daily.plist $HOME/Library/LaunchAgents/
launchctl load -w $HOME/Library/LaunchAgents/com.nekrald.lock.daily.plist

