#!/usr/bin/env bash
set -uexo pipefail
launchctl unload -w $HOME/Library/LaunchAgents/com.nekrald.lock.daily.plist
rm -rf $HOME/Library/LaunchAgents/com.nekrald.lock.daily.plist 
sudo unlink /usr/local/bin/lock_daily.py

