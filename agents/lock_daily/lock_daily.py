#!/usr/bin/env python3
import argparse
import logging
import subprocess as sp
from datetime import datetime
from datetime import timedelta


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("start", type=int, help='Time to start blocking.')
    parser.add_argument("end", type=int, help='Time to stop blocking.')
    return parser.parse_args()


def main():
    """ Main entry point. """
    args = parse_arguments()
    _process_time(args.start, args.end)


def _is_in_interval(start_hour: int, finish_hour: int) -> bool:
    """ Checks if current time is in the interval from start_hour to finish_hour. """

    start_hour, finish_hour = (start_hour + 24) % 24, (finish_hour + 24) % 24
    assert start_hour != finish_hour
    if finish_hour < start_hour:
        finish_hour += 24
    time_now = datetime.now()
    for hour_now in [time_now.hour, time_now.hour + 24]:
        if hour_now >= start_hour and hour_now < finish_hour:
            return True
    return False


def _process_time(start_hour: int, finish_hour: int) -> None:
    """ Locks screen if interval has started.
    Sends notification if interval is soon.
    """
    if _is_in_interval(start_hour, finish_hour):
        _minimize_finder()
        _lock_screen()
        _kill_terminal()
        return
    if _is_in_interval(start_hour - 1, start_hour):
        distance = 60 - datetime.now().minute
        if distance in [30, 25, 20, 15, 10, 5, 3]:
            _notify(distance)


def _notify(in_minutes: int) -> None:
    """ Notifies if break is soon. """
    process = sp.Popen([
        'osascript', '-e',
        'display notification "In {} minutes screen will be locked!"'.format(
                in_minutes)
    ], stderr=sp.PIPE)
    process.wait()


def _lock_screen() -> None:
    """ Locks screen. """
    process = sp.Popen([
        'osascript', '-e',
        'tell application "System Events"  to keystroke "q" using {command down,control down}'
    ], stderr=sp.PIPE)
    process.wait()


def _kill_terminal() -> None:
    """ Kills terminal. """
    process = sp.Popen([
        'killall', 'Terminal'], stderr=sp.PIPE)
    process.wait()


def _minimize_finder() -> None:
    """ Minimizes finder. """
    process = sp.Popen(['osascript', '-e',
        'tell application "Finder" to set collapsed of windows to true'],
        stderr=sp.PIPE)
    process.wait()


if __name__ == '__main__':
    main()
