#!/usr/bin/env python3
import argparse
import logging
import subprocess as sp
from datetime import datetime
from datetime import timedelta


def main():
    """ Main entry point. """
    kill_processes()


def kill_processes() -> None:
    PROCESS_LIST = [
        'Safari', 'safari',
        'Opera', 'opera',
        'Dash', 'dash',
        'links',
        'lynx',
        'w3m',
        'elinks',
        'links2',
        'Brave', 'brave',
        'Chrome', 'chrome', 'google-chrome',
        'Chromium', 'chromium', 'chromium-browser',
        'Telegram', 'telegram',
        'Discord', 'discord',
        'Vivaldi', 'vivaldi',
        'OmniWeb', 'omniweb',
        'Edge', 'edge',
        'SeaMonkey', 'seamonkey',
        'Maxthon', 'maxthon',
        'Roccat', 'roccat',
        'GNOME Web', 'gnome-browser', 'gnome-web', 'epiphany', 'epiphany-browser',
        'Internet Explorer', 'iexplore', 'iexplorer', 'internet-explorer'
    ]
    for item in PROCESS_LIST:
        process = sp.Popen([
            'killall', item], stderr=sp.PIPE)
        process.wait()


if __name__ == '__main__':
    main()
