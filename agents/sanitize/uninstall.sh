#!/usr/bin/env bash
set -uexo pipefail
launchctl unload -w com.nekrald.sanitize.plist
sudo unlink /usr/local/bin/sanitize.py
rm -rf $HOME/Library/LaunchAgents/com.nekrald.sanitize.plist 

