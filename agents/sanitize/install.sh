#!/usr/bin/env bash
directory=`pwd`
echo ${director}
set -uexo pipefail
sudo ln -sf "${directory}/sanitize.py"  /usr/local/bin/sanitize.py
mkdir -p /Users/nekrald/Library/LaunchAgents
cp com.nekrald.sanitize.plist $HOME/Library/LaunchAgents/
launchctl load -w com.nekrald.sanitize.plist

