alias ls='ls -FG'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias links='ls'
alias elinks='ls'
alias links2='ls'
alias lynx='ls'
alias w3m='ls'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias cp='cp -r'
alias rm='rm -r'
alias scp='scp -r'
alias df='df -h'
alias du='du -h'
alias vim='mvim -v'

# Colorized prompt
if [ $UID != 0  ] ; then
	PS1="\[\e[32;1m\]\u@\[\e[31;1m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ "
else
	PS1="\[\e[31;1m\]\u@\[\e[31;1m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ "
fi

# Colorized grep
export GREP_OPTIONS="--color=auto"
export GREP_COLOR="1;32"

