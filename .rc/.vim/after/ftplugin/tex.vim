setlocal nospell
let b:tex_flavor = 'pdflatex'
compiler tex
set errorformat=%f:%l:\ %m
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal autoindent
setlocal expandtab
setlocal smartindent
setlocal wildmenu
setlocal cin
setlocal showcmd

nmap <F9> :w <enter> : !make <enter>
setlocal ruler
setlocal number
