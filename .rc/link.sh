#!/usr/bin/env sh
CONFIG_PATH="$HOME/.rc"

ITEM_LIST=(.vimrc .bashrc .profile .ideavimrc
.screenrc .bash_aliases .ssh .vim  
)

set -uexo pipefail

cd $HOME

for item in ${ITEM_LIST[@]}; do
	rm -rf $item
	ln -sf "$CONFIG_PATH/$item"
done

if [ ! -d "$HOME/YouCompleteMe" ]; then
    git clone https://github.com/ycm-core/YouCompleteMe.git
    cd YouCompleteMe
    git submodule update --init --recursive
    ./install.py --clang-completer --ts-completer
fi

